# Copyright (C) 2012 Software in the Public Interest, SPI Inc.
#
# Changes:
# - Initial translation
#      Fernando C. Estrada <fcestrada@fcestrada.com> , 2012
#
#  Traductores, si no conoce el formato PO, merece la pena leer la
#  documentación de gettext, especialmente las secciones dedicadas a este
#  formato, por ejemplo ejecutando:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
#   https://www.debian.org/intl/spanish/
#   especialmente las notas y normas de traducción en
#   https://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
#   /usr/share/doc/po-debconf/README-trans
#   o https://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Si tiene dudas o consultas sobre esta traducción consulte con el último
# traductor (campo Last-Translator) y ponga en copia a la lista de
# traducción de Debian al español (<debian-l10n-spanish@lists.debian.org>)
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Website\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2018-06-09 18:51+0200\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian L10N Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Estadísticas de traducción del sitio web de Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Hay %d páginas por traducir."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Hay %d bytes por traducir."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Hay %d cadenas de caracteres por traducir."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Versión incorrecta de traducción"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Esta traducción está muy desactualizada"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "La original es más nueva que esta traducción"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "La original ya no existe"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "contador de visitas N/A"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "visitas"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Haga clic para obtener la información de diffstat"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Creado con <transstatslink>"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Resumen de traducción para"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Sin traducir"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Desactualizado"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Traducido"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Actualizado"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "ficheros"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Nota: el listado de páginas está ordenado por popularidad. Coloque el ratón "
"sobre el nombre de la página para ver el número de visitas."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Traducciones desactualizadas"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Fichero"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diferencias"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Comentarios"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "línea de orden «git»"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Registro"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Traducción"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Encargado"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Estado"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Traductor"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Fecha"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Páginas generales no traducidas"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Páginas generales sin traducir"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Elementos de noticias no traducidos"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Elementos de noticias sin traducir"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Páginas de consultores/usuarios no traducidas"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Páginas de consultores/usuarios sin traducir"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Páginas internacionales no traducidas"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Páginas internacionales sin traducir"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Páginas traducidas (actualizadas)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Plantillas traducidas (ficheros PO)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Estadísticas de traducción de ficheros PO"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Impreciso"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Sin traducir"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Páginas web traducidas"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Estadísticas de traducción por número de páginas"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Idioma"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Traducciones"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Páginas web traducidas (por tamaño)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Estadísticas de traducción por tamaño de las páginas"

#~ msgid "Created with"
#~ msgstr "Creado por"

#~ msgid "Commit diff"
#~ msgstr "Diff de los cambios"

#~ msgid "Colored diff"
#~ msgstr "Diff coloreado"

#~ msgid "Unified diff"
#~ msgstr "Diff unificado"
