<define-tag pagetitle>Updated Debian 9: 9.10 released</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the tenth update of its
oldstable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Miscellaneous Bugfixes</h2>

<p>This oldstable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction base-files "Update for the point release; add VERSION_CODENAME to os-release">
<correction basez "Properly decode base64url encoded strings">
<correction biomaj-watcher "Fix upgrades from jessie to stretch">
<correction c-icap-modules "Add support for clamav 0.101.1">
<correction chaosreader "Add missing dependency on libnet-dns-perl">
<correction clamav "New upstream stable release: add scan time limit to mitigate against zip-bombs [CVE-2019-12625]; fix out-of-bounds write within the NSIS bzip2 library [CVE-2019-12900]">
<correction corekeeper "Do not use a world-writable /var/crash with the dumper script; handle older versions of the Linux kernel in a safer way; do not truncate core names for executables with spaces">
<correction cups "Fix multiple security/disclosure issues - SNMP buffer overflows [CVE-2019-8696 CVE-2019-8675], IPP buffer overflow, Denial of Service and memory disclosure issues in the scheduler">
<correction dansguardian "Add support for clamav 0.101">
<correction dar "Rebuild to update <q>built-using</q> packages">
<correction debian-archive-keyring "Add buster keys; remove wheezy keys">
<correction fence-agents "Fix denial of service issue [CVE-2019-10153]">
<correction fig2dev "Do not segfault on circle/half circle arrowheads with a magnification larger than 42 [CVE-2019-14275]">
<correction fribidi "Fix right-to-left output in debian-installer text mode">
<correction fusiondirectory "Stricter checks on LDAP lookups; add missing dependency on php-xml">
<correction gettext "Stop xgettext() from crashing when run with --its=FILE option">
<correction glib2.0 "Create directory and file with restrictive permissions when using the GKeyfileSettingsBackend [CVE-2019-13012]; avoid buffer read overrun when formatting error messages for invalid UTF-8 in GMarkup [CVE-2018-16429]; avoid NULL dereference when parsing invalid GMarkup with a malformed closing tag not paired with an opening tag [CVE-2018-16429]">
<correction gocode "gocode-auto-complete-el: Make pre-dependency on auto-complete-el versioned to fix upgrades from jessie to stretch">
<correction groonga "Mitigate privilege escalation by changing the owner and group of logs with <q>su</q> option">
<correction grub2 "Fixes for Xen UEFI support">
<correction gsoap "Fix denial of service issue if a server application is built with the -DWITH_COOKIES flag [CVE-2019-7659]; fix issue with DIME protocol receiver and malformed DIME headers">
<correction gthumb "Fix double-free bug [CVE-2018-18718]">
<correction havp "Add support for clamav 0.101.1">
<correction icu "Fix segfault in pkgdata command">
<correction koji "Fix SQL injection issue [CVE-2018-1002161]; properly validate SCM paths [CVE-2017-1002153]">
<correction lemonldap-ng "Fix cross-domain authentication regression; fix XML external entity vulnerability">
<correction libcaca "Fix integer overflow issues [CVE-2018-20545 CVE-2018-20546 CVE-2018-20547 CVE-2018-20548 CVE-2018-20549]">
<correction libclamunrar "New upstream stable release">
<correction libconvert-units-perl "No-change rebuild with fixed version number">
<correction libdatetime-timezone-perl "Update included data">
<correction libebml "Apply upstream fixes for heap-based buffer over-reads">
<correction libevent-rpc-perl "Fix build failure due to expired test SSL certificates">
<correction libgd2 "Fix uninitialized read in gdImageCreateFromXbm [CVE-2019-11038]">
<correction libgovirt "Re-generate test certificates with expiration date far in the future to avoid test failures">
<correction librecad "Fix denial of service via crafted file [CVE-2018-19105]">
<correction libsdl2-image "Fix multiple security issues">
<correction libthrift-java "Fix bypass of SASL negotiation [CVE-2018-1320]">
<correction libtk-img "Stop using internal copies of JPEG, Zlib and PixarLog codecs, fixing crashes">
<correction libu2f-host "Fix stack memory leak [CVE-2019-9578]">
<correction libxslt "Fix security framework bypass [CVE-2019-11068]; fix uninitialized read of xsl:number token [CVE-2019-13117]; fix uninitialized read with UTF-8 grouping chars [CVE-2019-13118]">
<correction linux "New upstream version with ABI bump; security fixes [CVE-2015-8553 CVE-2017-5967 CVE-2018-20509 CVE-2018-20510 CVE-2018-20836 CVE-2018-5995 CVE-2019-11487 CVE-2019-3882]">
<correction linux-latest "Update for 4.9.0-11 kernel ABI">
<correction liquidsoap "Fix compilation with Ocaml 4.02">
<correction llvm-toolchain-7 "New package to support building new Firefox versions">
<correction mariadb-10.1 "New upstream stable release; security fixes [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2805 CVE-2019-2627 CVE-2019-2614]">
<correction minissdpd "Prevent a use-after-free vulnerability that would allow a remote attacker to crash the process [CVE-2019-12106]">
<correction miniupnpd "Fix denial of service issues [CVE-2019-12108 CVE-2019-12109 CVE-2019-12110]; fix information leak [CVE-2019-12107]">
<correction mitmproxy "Blacklist tests that require Internet access; prevent insertion of unwanted upper-bound versioned dependencies">
<correction monkeysphere "Fix build failure by updating the tests to accommodate an updated GnuPG in stretch now producing a different output">
<correction nasm-mozilla "New package to support building new Firefox versions">
<correction ncbi-tools6 "Repackage without non-free data/UniVec.*">
<correction node-growl "Sanitize input before passing it to exec">
<correction node-ws "Restrict upload size [CVE-2016-10542]">
<correction open-vm-tools "Fix possible security issue with the permissions of the intermediate staging directory and path">
<correction openldap "Restrict rootDN proxyauthz to its own databases [CVE-2019-13057]; enforce sasl_ssf ACL statement on every connection [CVE-2019-13565]; fix slapo-rwm to not free original filter when rewritten filter is invalid">
<correction openssh "Fix deadlock in key matching">
<correction passwordsafe "Don't install localization files under an extra subdirectory">
<correction pound "Fix request smuggling via crafted headers [CVE-2016-10711]">
<correction prelink "Rebuild to update <q>built-using</q> packages">
<correction python-clamav "Add support for clamav 0.101.1">
<correction reportbug "Update release names, following buster release">
<correction resiprocate "Resolve an installation issue with libssl-dev and --install-recommends">
<correction sash "Rebuild to update <q>built-using</q> packages">
<correction sdl-image1.2 "Fix buffer overflows [CVE-2018-3977 CVE-2019-5058 CVE-2019-5052], out-of-bounds access [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction signing-party "Fix unsafe shell call enabling shell injection via a User ID [CVE-2019-11627]">
<correction slurm-llnl "Fix potential heap overflow on 32-bit systems [CVE-2019-6438]">
<correction sox "Fix several security issues [CVE-2019-8354 CVE-2019-8355 CVE-2019-8356 CVE-2019-8357 927906 CVE-2019-1010004 CVE-2017-18189 881121 CVE-2017-15642 882144 CVE-2017-15372 878808 CVE-2017-15371 878809 CVE-2017-15370 878810 CVE-2017-11359 CVE-2017-11358 CVE-2017-11332">
<correction systemd "Do not stop ndisc client in case of configuration error">
<correction t-digest "No-change rebuild to avoid re-use of pre-epoch version 3.0-1">
<correction tenshi "Fix PID file issue that allows local users to kill arbitrary processes [CVE-2017-11746]">
<correction tzdata "New upstream release">
<correction unzip "Fix incorrect parsing of 64-bit values in fileio.c; fix zip-bomb issues [CVE-2019-13232]">
<correction usbutils "Update USB ID list">
<correction xymon "Fix several (server only) security issues [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubico-piv-tool "Fix security issues [CVE-2018-14779 CVE-2018-14780]">
<correction z3 "Do not set the SONAME of libz3java.so to libz3.so.4">
<correction zfs-auto-snapshot "Make cron jobs exit silently after package removal">
<correction zsh "Rebuild to update <q>built-using</q> packages">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the oldstable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2019 4435 libpng1.6>
<dsa 2019 4436 imagemagick>
<dsa 2019 4437 gst-plugins-base1.0>
<dsa 2019 4438 atftp>
<dsa 2019 4439 postgresql-9.6>
<dsa 2019 4440 bind9>
<dsa 2019 4441 symfony>
<dsa 2019 4442 cups-filters>
<dsa 2019 4442 ghostscript>
<dsa 2019 4443 samba>
<dsa 2019 4444 linux>
<dsa 2019 4445 drupal7>
<dsa 2019 4446 lemonldap-ng>
<dsa 2019 4447 intel-microcode>
<dsa 2019 4448 firefox-esr>
<dsa 2019 4449 ffmpeg>
<dsa 2019 4450 wpa>
<dsa 2019 4451 thunderbird>
<dsa 2019 4452 jackson-databind>
<dsa 2019 4453 openjdk-8>
<dsa 2019 4454 qemu>
<dsa 2019 4455 heimdal>
<dsa 2019 4456 exim4>
<dsa 2019 4457 evolution>
<dsa 2019 4458 cyrus-imapd>
<dsa 2019 4459 vlc>
<dsa 2019 4460 mediawiki>
<dsa 2019 4461 zookeeper>
<dsa 2019 4462 dbus>
<dsa 2019 4463 znc>
<dsa 2019 4464 thunderbird>
<dsa 2019 4465 linux>
<dsa 2019 4466 firefox-esr>
<dsa 2019 4467 vim>
<dsa 2019 4468 php-horde-form>
<dsa 2019 4469 libvirt>
<dsa 2019 4470 pdns>
<dsa 2019 4471 thunderbird>
<dsa 2019 4472 expat>
<dsa 2019 4473 rdesktop>
<dsa 2019 4475 openssl>
<dsa 2019 4475 openssl1.0>
<dsa 2019 4476 python-django>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4485 openjdk-8>
<dsa 2019 4487 neovim>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4492 postgresql-9.6>
<dsa 2019 4494 kconfig>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4506 qemu>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Removed packages</h2>

<p>The following packages were removed due to circumstances beyond our control:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction pump "Unmaintained; security issues">
<correction teeworlds "Security issues; incompatible with current servers">

</table>

<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into oldstable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current oldstable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Proposed updates to the oldstable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>oldstable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>
