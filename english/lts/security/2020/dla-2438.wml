<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two heap overflow vulnerabilities in
raptor2, a set of parsers for RDF files that is used, amongst others, in
LibreOffice.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18926">CVE-2017-18926</a>

    <p>raptor_xml_writer_start_element_common in raptor_xml_writer.c in Raptor
    RDF Syntax Library 2.0.15 miscalculates the maximum nspace declarations for
    the XML writer, leading to heap-based buffer overflows (sometimes seen in
    raptor_qname_format_as_xml).</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.0.14-1+deb9u1.</p>

<p>We recommend that you upgrade your raptor2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2438.data"
# $Id: $
