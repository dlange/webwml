<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A minor security issue and a severe packaging bug have been fixed in
tinyproxy, a lightweight http proxy daemon.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11747">CVE-2017-11747</a>

  <p>main.c in Tinyproxy created a /var/run/tinyproxy/tinyproxy.pid file
  after dropping privileges to a non-root account, which might have allowed
  local users to kill arbitrary processes by leveraging access to this
  non-root account for tinyproxy.pid modification before a root script
  executed a "kill `cat /run/tinyproxy/tinyproxy.pid`" command.</p>

<p>OTHER</p>

  <p>Furthermore, a severe flaw had been discovered by Tim Duesterhus in
  Debian's init script for tinyproxy. With the tiny.conf configuration
  file having the PidFile option removed, the next run of logrotate (if
  installed) would have changed the owner of the system's base directory
  ("/") to tinyproxy:tinyproxy.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.8.3-3+deb8u1.</p>

<p>We recommend that you upgrade your tinyproxy packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2163.data"
# $Id: $
