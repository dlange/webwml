<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>libplist is a library for reading and writing the Apple binary and XML
property lists format. It's part of the libimobiledevice stack, providing
access to iDevices (iPod, iPhone, iPad ...).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5209">CVE-2017-5209</a>

    <p>The base64decode function in base64.c allows attackers to obtain sensitive
    information from process memory or cause a denial of service (buffer
    over-read) via split encoded Apple Property List data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5545">CVE-2017-5545</a>

    <p>The main function in plistutil.c allows attackers to obtain sensitive
    information from process memory or cause a denial of service (buffer
    over-read) via Apple Property List data that is too short.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5834">CVE-2017-5834</a>

    <p>The parse_dict_node function in bplist.c allows attackers to cause a denial
    of service (out-of-bounds heap read and crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5835">CVE-2017-5835</a>

    <p>libplist allows attackers to cause a denial of service (large memory
    allocation and crash) via vectors involving an offset size of zero.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6435">CVE-2017-6435</a>

    <p>The parse_string_node function in bplist.c allows local users to cause a
    denial of service (memory corruption) via a crafted plist file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6436">CVE-2017-6436</a>

    <p>The parse_string_node function in bplist.c allows local users to cause a
    denial of service (memory allocation error) via a crafted plist file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6439">CVE-2017-6439</a>

    <p>Heap-based buffer overflow in the parse_string_node function in bplist.c
    allows local users to cause a denial of service (out-of-bounds write) via
    a crafted plist file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7982">CVE-2017-7982</a>

    <p>Integer overflow in the plist_from_bin function in bplist.c allows remote
    attackers to cause a denial of service (heap-based buffer over-read and
    application crash) via a crafted plist file.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.11-3+deb8u1.</p>

<p>We recommend that you upgrade your libplist packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2168.data"
# $Id: $
