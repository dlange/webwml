<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that the sudo noexec restriction could have been
bypassed if application run via sudo executed system(), popen() or
wordexp() C library functions with a user supplied argument. A local
user permitted to run such application via sudo with noexec
restriction could possibly use this flaw to execute arbitrary commands
with elevated privileges.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7032">CVE-2016-7032</a>

    <p>noexec bypass via system() and popen()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7076">CVE-2016-7076</a>

    <p>noexec bypass via wordexp()</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.5p2-1+nmu3+deb7u2.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-707.data"
# $Id: $
