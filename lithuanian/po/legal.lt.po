#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-02-22 14:56+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "Licencijos informacija"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr "DSL Indeksas"

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DFSG"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "DFSG DUK"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr "Debian-Legal Archyvas"

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr "%s  &ndash; %s: %s"

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr "%s  &ndash; %s, Versija %s: %s"

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "Paskelbimo data"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "Licencija"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "Versija"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "Santrauka"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr "Pateisinimas"

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "Diskusija"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "Originali santrauka"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"Originalios santraukos autorius <summary-author/> galima rasti <a href="
"\"<summary-url/>\"> konferencijų archyvuose</a>."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "Šią santrauką paruošė <summary-author/>."

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "Licencijos tekstas (išverstas)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "Licencijos tekstas"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "laisva"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "nelaisva"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "su platinimo teisių apribojimais"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Laisva"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "Nelaisva"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "Su platinimo teisių apribojimais"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"Žiūrėkit <a href=\"./\">licencijos informacijos</a> puslapį dėl Debian "
"Licencijos Santraukos (DLS)."
