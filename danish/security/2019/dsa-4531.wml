#use wml::debian::translation-check translation="5ff7a077f0e13d9fe9cae52a517d81e9a15c05d7" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14821">CVE-2019-14821</a>

    <p>Matt Delco rapporterede om en kapløbstilstand i KVM's coalescede 
    MMIO-facilitet, hvilket kunne medføre tilgang udenfor grænserne i kernen.  
    En lokal angriber, med rettigheder til at tilgå /dev/kvm, kunne udnytte 
    fejlen til at forårsage et lammelsesangreb (hukommelseskorruption eller 
    nedbrud) eller muligvis itl rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14835">CVE-2019-14835</a>

    <p>Peter Pi fra Tencent Blade Team opdagede en manglende grænsekontrol i 
    vhost_net, netværksbackenddriveren til KVM-værter, førende til et 
    bufferoverløb, når værten påbegyndte en livemigrering af en VM.  En 
    angriber med kontrol over en VM, kunne udnytte fejlen til at forårsage 
    et lammelsesangreb (hukommelseskorruption eller nedbrud) eller muligvis 
    til rettighedsforøgelse på værten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15117">CVE-2019-15117</a>

    <p>Hui Peng og Mathias Payer rapporterede om en manglende grænsekontrol i 
    usb-audio-driverens kode til descriptorfortolkning, førende til en 
    bufferoverlæsning.  En angriber, der var i stand til at tilføje USB-enheder, 
    kunne muligvis udnytte fejlen til at forårsage et lammelsesangreb 
    (nedbrud).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15118">CVE-2019-15118</a>

    <p>Hui Peng og Mathias Payer rapporterede om ubegrænset rekursion i 
    usb-audio-driverens kode til descriptorfortolkning, førende til et 
    stakoverløb.  En angriber, der var i stand til at tilføje USB-enheder, 
    kunne forårsage et lammelsesangreb (hukommelseskorruption eller nedbrud) 
    eller muligvis rettighedsforøgelse.  På arkitekturen amd64, og på 
    arkitekturen arm64 i buster, er det løst med en guardpage i kernestakken, så 
    det kun er muligt at forårsage et nedbrud.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15902">CVE-2019-15902</a>

    <p>Brad Spengler rapporterede at en tilbageførselsfejl, genindførte en 
    spectre-v1-sårbarhed i ptrace-undersystemet i funktionen 
    ptrace_get_debugreg().</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 4.9.189-3+deb9u1.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 4.19.67-2+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4531.data"
