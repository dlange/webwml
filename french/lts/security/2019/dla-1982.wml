#use wml::debian::translation-check translation="c15f8e07f925afb0adb68cc73c05e47395a73a93" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans OpenAFS, un
système de fichiers distribué.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18601">CVE-2019-18601</a>

<p>OpenAFS est prédisposé à un déni de service à partir d’un accès à des données
non sérialisées car des attaquants distants peuvent créer une série d’appels
VOTE_Debug RPC pour planter le serveur de base de données dans le gestionnaire
RPC SVOTE_Debug.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18602">CVE-2019-18602</a>

<p>OpenAFS est prédisposé à une vulnérabilité de divulgation d'informations car
des scalaires non initialisés sont envoyés par le réseau à un pair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18603">CVE-2019-18603</a>

<p>OpenAFS est prédisposé à une fuite d'informations dans certaines conditions
d’erreurs car des variables RPC de sortie non initialisées sont envoyées par
le réseau à un pair.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.6.9-2+deb8u9.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openafs.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1982.data"
# $Id: $
