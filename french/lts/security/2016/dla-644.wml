#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libav :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1872">CVE-2015-1872</a>

<p>La fonction ff_mjpeg_decode_sof dans libavcodec/mjpegdec.c dans Libav
antérieur à 0.8.18 ne vérifiait pas le nombre de composants dans le
segment démarrage de trame JPEG-LS. Cela permet à des attaquants distants
de provoquer un déni de service (accès tableau hors limites) ou
éventuellement d’avoir un impact non précisé à l’aide de données Motion
JPEG contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5479">CVE-2015-5479</a>

<p>La fonction ff_h263_decode_mba dans libavcodec/ituh263dec.c dans Libav
antérieur à 11.5 permet à des attaquants distants de provoquer un déni de
service (erreur de division par zéro et plantage de l'application) à l'aide
d'un fichier avec des dimensions contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7393">CVE-2016-7393</a>

<p>La fonction aac_sync dans libavcodec/aac_parser.c dans Libav antérieur
à 11.5 est vulnérable à un dépassement de pile.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 6:0.8.18-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-644.data"
# $Id: $
