#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans libxslt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7995">CVE-2015-7995</a>

<p>L'absence de vérification de type pourrait provoquer le plantage de
l'application à l'aide d'un fichier contrefait pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1683">CVE-2016-1683</a>

<p>Un bogue d'accès au tas hors limites a été découvert dans libxslt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1684">CVE-2016-1684</a>

<p>Il y a un bogue de dépassement d'entier dans libxslt qui pourrait
conduire à un plantage de l'application.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.1.26-14.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxslt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-514.data"
# $Id: $
