#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La bibliothèque libtiff et les utilitaires associés fournis dans
libtiff-tools sont vulnérables à de nombreux problèmes de sécurité.</p>

<p>Cette mise à jour retire de nombreux utilitaires qui ne sont plus pris
en charge par l'amont et qui sont affectés par de multiples problèmes de
corruption de mémoire :</p>

<ul>
<li>bmp2tiff (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3619">CVE-2016-3619</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3620">CVE-2016-3620</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3621">CVE-2016-3621</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5319">CVE-2016-5319</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2015-8668">CVE-2015-8668</a>)</li>
<li>gif2tiff (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3186">CVE-2016-3186</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5102">CVE-2016-5102</a>)</li>
<li>ras2tiff</li>
<li>sgi2tiff</li>
<li>sgisv</li>
<li>ycbcr</li>
<li>rgb2ycbcr (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3623">CVE-2016-3623</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3624">CVE-2016-3624</a>)</li>
<li>thumbnail (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3631">CVE-2016-3631</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3632">CVE-2016-3632</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3633">CVE-2016-3633</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3634">CVE-2016-3634</a> et
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8331">CVE-2016-8331</a>)</li>
</ul>

<p>Cette mise à jour corrige aussi les problèmes suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-8128">CVE-2014-8128</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2015-7554">CVE-2015-7554</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5318">CVE-2016-5318</a>

<p>Plusieurs dépassements de tampon déclenchés par TIFFGetField() sur des
étiquettes inconnues. En l'absence de correctif de l'amont, la liste des
étiquettes connues a été étendue pour couvrir toutes celles qui sont en
usage dans les utilitaires de TIFF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5652">CVE-2016-5652</a>

<p>Dépassement de tas dans tiff2pdf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6223">CVE-2016-6223</a>

<p>Fuite d'information dans libtiff/tif_read.c. Correction de lecture hors
limites dans des fichiers mappés en mémoire dans TIFFReadRawStrip1() et
TIFFReadRawTile1() quand StripOffset est au-delà de la valeur maximale de
tmsize_t (problème signalé par Mathias Svensson).</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.0.2-6+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-693.data"
# $Id: $
