msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-02-16 17:43+0100\n"
"Last-Translator: Andreas Rönnquist <gusnan@openmailbox.org>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Distributör"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Tillåter bidrag"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arkitekturer"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Levererar internationellt"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontakt"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Hemsida för distributör"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "sida"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "e-post"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "inom Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "inom vissa områden"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "källkod"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "och"

#~ msgid "Vendor:"
#~ msgstr "Distributör:"

#~ msgid "URL for Debian Page:"
#~ msgstr "Webbadress för Debiansida:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Tillåter bidrag till Debian:"

#~ msgid "Country:"
#~ msgstr "Land:"

#~ msgid "Ship International:"
#~ msgstr "Levererar internationellt:"

#~ msgid "email:"
#~ msgstr "E-post:"

#~ msgid "CD Type:"
#~ msgstr "Typ av cd:"

#~ msgid "DVD Type:"
#~ msgstr "Typ av dvd:"

#~ msgid "Architectures:"
#~ msgstr "Arkitekturer:"

#~ msgid "Official CD"
#~ msgstr "Officiell cd"

#~ msgid "Official DVD"
#~ msgstr "Officiell dvd"

#~ msgid "Development Snapshot"
#~ msgstr "Utvecklarversion"

#~ msgid "Vendor Release"
#~ msgstr "Distributörspecifik version"

#~ msgid "Multiple Distribution"
#~ msgstr "Samdistribuerad"

#~ msgid "non-US included"
#~ msgstr "inkluderar non-US-delen"

#~ msgid "non-free included"
#~ msgstr "inkluderar non-free-delen"

#~ msgid "contrib included"
#~ msgstr "inkluderar contrib-delen"

#~ msgid "vendor additions"
#~ msgstr "distributörtillägg"

#~ msgid "Custom Release"
#~ msgstr "Specialgjord version"

#~ msgid "reseller of $var"
#~ msgstr "återförsäljare för $var"

#~ msgid "reseller"
#~ msgstr "återförsäljare"

#~ msgid "updated weekly"
#~ msgstr "uppdateras veckovis"

#~ msgid "updated twice weekly"
#~ msgstr "uppdateras två gånger i veckan"

#~ msgid "updated monthly"
#~ msgstr "uppdateras månadsvis"
